export const API_BACK = "http://localhost:8000/api";
export const API_INTERVIEWS = API_BACK + "/interviews/";
export const API_INTERVIEW_REPORT = API_BACK + "/interview_reports/";
export const API_QUESTIONS = API_BACK + "/questions/";
export const API_CHALLENGES = API_BACK + "/challenges/";
