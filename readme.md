#### Repo

-  https://github.com/centrodph/fullstack-tp

-  Front End: http://localhost:3000
-  Back End: http://localhost:8000
    -  Admin: http://localhost:8000/admin
    -  Docs: http://localhost:8000/doc
    -  Open API: http://localhost:8000/api

#### Setup steps

-  yarn front:install
-  yarn server:install
-  yarn front:start
-  yarn server:migrate
-  yarn server:start

#### Development

-  yarn front:start
-  yarn server:start

#### Users

- admin admin

- User-Interviewer 8kLNEheFS@2M7PT